variable "mykey" {}
variable "ami" {
  description = "Amazon Linux 2"
}
variable "region" {}
variable "instance_type" {}
variable "mentor-server-secgr" {}
variable "mentor-server-tag" {}
